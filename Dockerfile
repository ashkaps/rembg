# FROM nvidia/cuda

# RUN apt-get update && apt-get install -y --no-install-recommends python3 python3-pip python3-dev llvm llvm-dev
# RUN pip3 install rembg

# ENTRYPOINT ["rembg"]
# CMD []

FROM python:3.8
# FROM nvidia/cuda
# RUN apt-get update && apt-get install -y --no-install-recommends python3 python3-pip python3-dev llvm llvm-dev libgl1-mesa-glx
# RUN sudo apt-get install nvidia-cuda-toolkit
COPY . /app
WORKDIR /app
# RUN pip3 install -r requirements.txt 
RUN pip3 install -r requirements.txt

EXPOSE 5000
ENTRYPOINT [ "python3" ] 
CMD [ "src/rembg/server.py" ] 