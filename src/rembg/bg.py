import functools
import io

import numpy as np
from PIL import Image
from pymatting.alpha.estimate_alpha_cf import estimate_alpha_cf
# from pymatting.alpha.estimate_alpha_rw import estimate_alpha_rw
from pymatting.foreground.estimate_foreground_ml import estimate_foreground_ml
from pymatting.util.util import stack_images
from scipy.ndimage.morphology import binary_erosion
# from imageai.Detection import ObjectDetection
import os
import datetime
from u2net import detect

IMG_TYPE = Image.BICUBIC

def resize(img):
    base_size = img.size
    
    w,h = img.size
    if w > 1200 or h > 1200:
        img = img.resize((int(w*(0.6)),int(h*(0.6))), IMG_TYPE)
    else:    
        img = img.resize(base_size, IMG_TYPE)
    print('resizing: ' + str(base_size) + '. resized: ' + str(img.size))
    return img
    
    # return resize(img)


def alpha_matting_cutout(
    img,
    mask,
    foreground_threshold,
    background_threshold,
    erode_structure_size,
):
    print('>>>>>>>>>>>>> alpha matting ->> ' + str(datetime.datetime.now()))
    base_size = (2000, 2000)
    # base_size = img.size
    w,h = img.size
    size = img.size
    # print(size)
    # img = resize(img)
    # img = resize(img)
    print('resizing image ->> ' + str(datetime.datetime.now()))
    # if w > 1200 or h > 1200:
    #     img = img.resize((int(w*(0.4)),int(h*(0.4))), IMG_TYPE)
    # else:    
    #     img = img.resize(base_size, IMG_TYPE)
    print('iamge resized. resizing mask ->> ' + str(datetime.datetime.now()))
    mask = mask.resize(img.size, IMG_TYPE)
    print('mask resized ->> ' + str(datetime.datetime.now()))
    # img.save('img_resizw.png')
    # mask.save('img_mask.png')
    print('img to array ->> ' + str(datetime.datetime.now()))

    img = np.asarray(img)
    print('mask to array ->> ' + str(datetime.datetime.now()))
    mask = np.asarray(mask)

    # guess likely foreground/background
    is_foreground = mask > foreground_threshold
    is_background = mask < background_threshold

    # erode foreground/background
    structure = None
    if erode_structure_size > 0:
        print('eroding ->> ' + str(datetime.datetime.now()))
        structure = np.ones((erode_structure_size, erode_structure_size), dtype=np.int)
        print('erosion done ->> ' + str(datetime.datetime.now()))

    print('is foreground ->> ' + str(datetime.datetime.now()))
    is_foreground = binary_erosion(is_foreground, structure=structure)
    print('is foredground done. is background ->> ' + str(datetime.datetime.now()))
    is_background = binary_erosion(is_background, structure=structure, border_value=1)
    print('isbackground done ->> ' + str(datetime.datetime.now()))
    # build trimap
    # 0   = background
    # 128 = unknown
    # 255 = foreground
    trimap = np.full(mask.shape, dtype=np.uint8, fill_value=128)
    trimap[is_foreground] = 255
    trimap[is_background] = 0

    # build the cutout image
    img_normalized = img / 255.0
    trimap_normalized = trimap / 255.0

    print('estimate alpha cf ->> ' + str(datetime.datetime.now()))
    # alpha = estimate_alpha_cf(img_normalized, trimap_normalized, laplacian_kwargs=dict(epsilon=1e-5))
    alpha = estimate_alpha_cf(img_normalized, trimap_normalized)
    print('esstimate alpa cf done ->> ' + str(datetime.datetime.now()))
    # alpha = estimate_alpha_cf(img_normalized, trimap_normalized)
    # alpha = estimate_alpha_rw(img_normalized, trimap_normalized)
    print('estimating foreground ml ->> ' + str(datetime.datetime.now()))
    foreground = estimate_foreground_ml(img_normalized, alpha)
    print('estimate foreground ml done ->> ' + str(datetime.datetime.now()))
    print('stacking images ->> ' + str(datetime.datetime.now()))
    cutout = stack_images(foreground, alpha)
    print('stacking images done ->> ' + str(datetime.datetime.now()))
    
    cutout = np.clip(cutout * 255, 0, 255).astype(np.uint8)
    print('cutout from array ->> ' + str(datetime.datetime.now()))
    cutout = Image.fromarray(cutout)
    # cutout.save('img_cutout_fromarray.png')
    print('resizing cutout ->> ' + str(datetime.datetime.now()))
    cutout = cutout.resize(size, IMG_TYPE)
    # cutout.save('img_cutout_resize.png')
    print('>>>>>>>>>>>>> alpha matting done ->> ' + str(datetime.datetime.now()))
    return cutout


def naive_cutout(img, mask):
    empty = Image.new("RGBA", (img.size), 0)
    cutout = Image.composite(img, empty, mask.resize(img.size, IMG_TYPE))
    return cutout


@functools.lru_cache(maxsize=None)
def get_model(model_name):
    if model_name == "u2netp":
        return detect.load_model(model_name="u2netp")
    else:
        return detect.load_model(model_name="u2net")


# def find_objects(img):
#     execution_path = os.getcwd()
#     detector = ObjectDetection()
#     detector.setModelTypeAsRetinaNet()
#     detector.setModelPath( os.path.join(execution_path , "resnet50_coco_best_v2.0.1.h5"))
#     detector.loadModel()
#     detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "img.png"), output_image_path=os.path.join(execution_path , "imagenew.jpg"))

#     for eachObject in detections:
#         print(eachObject["name"] , " : " , eachObject["percentage_probability"] )

def remove(
    data,
    model_name="u2netp",
    alpha_matting=True,
    alpha_matting_foreground_threshold=240,
    alpha_matting_background_threshold=10,
    alpha_matting_erode_structure_size=3,
):  
    print('getting model ->> ' + str(datetime.datetime.now()))
    model = get_model(model_name)
    print('model received ->> ' + str(datetime.datetime.now()))
    img = Image.open(io.BytesIO(data)).convert("RGB")
    w,h = img.size
    
    # img.save('img.png')
    # find_objects('img.png')
    print('detecting mask ->> ' + str(datetime.datetime.now()))
    mask = detect.predict(model, np.array(img)).convert("L")
    print('mast detected ->> ' + str(datetime.datetime.now()))

    if alpha_matting:
        cutout = alpha_matting_cutout(
            img,
            mask,
            alpha_matting_foreground_threshold,
            alpha_matting_background_threshold,
            alpha_matting_erode_structure_size,
        )
    else:
        print('naive cutout ->> ' + str(datetime.datetime.now()))
        cutout = naive_cutout(img, mask)
        
    print('cutout created ->> ' + str(datetime.datetime.now()))
    bio = io.BytesIO()
    cutout.save(bio, "PNG")

    return bio.getbuffer()
